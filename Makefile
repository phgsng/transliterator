module 		= transliterator
zipball		= t-transliterator.zip
doc-pdf		= transliterator.pdf
license		= COPYING
doc-src		= doc/transliterator.tex
interface-src	= src/t-transliterator.xml
mod-src		= src/transliterator.lua
mod-src		+= src/trans_tables_bg.lua
mod-src		+= src/trans_tables_glag.lua
mod-src		+= src/trans_tables_gr.lua
mod-src		+= src/trans_tables_iso9.lua
mod-src		+= src/trans_tables_scntfc.lua
mod-src		+= src/trans_tables_sr.lua
mod-src		+= src/trans_tables_trsc.lua
mod-src		+= src/t-transliterator.mkii
mod-src		+= src/t-transliterator.mkiv
mod-src		+= src/t-transliterator.tex
script-src	= scripts/mtx-t-transliterate.lua
src 		= $(license) $(doc-src) $(mod-src) $(script-src)
texmf-doc	= doc/context/third/transliterator
texmf-scripts	= scripts/context/lua/third/transliterator
texmf-interface	= tex/context/interface/third
texmf-tex	= tex/context/third/transliterator

# zip for upload on modules.contextgarden.net
texmf-zip: build/$(zipball)

doc: build/$(doc-pdf)

build/$(doc-pdf): $(doc-src)
	mkdir -p -- build
	( pushd build && context ../$(doc-src); )

build/$(zipball): $(src) doc
	mkdir -p -- build/texmf/$(texmf-doc)
	mkdir -p -- build/texmf/$(texmf-scripts)
	mkdir -p -- build/texmf/$(texmf-interface)
	mkdir -p -- build/texmf/$(texmf-tex)
	cp -f -- build/$(doc-pdf) build/texmf/$(texmf-doc)/
	cp -f -- $(doc-src) build/texmf/$(texmf-doc)/
	cp -f -- $(doc-src) build/texmf/$(texmf-doc)/
	cp -f -- $(script-src) build/texmf/$(texmf-scripts)/
	cp -f -- $(interface-src) build/texmf/$(texmf-interface)/
	cp -f -- $(mod-src) build/texmf/$(texmf-tex)/
	# some dancing around needed to avoid including the build prefix
	( pushd build/texmf && zip -r $(zipball) doc scripts tex; )
	mv -f build/texmf/$(zipball) build

clean:
	rm -rf -- build

.PHONY: clean

